package id.filkom.papb.recyclerview2051050401111053;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv1;
    EditText et_nama, et_nama2;
    Button btn_simpan;
    MahasiswaAdapter mahasiswaAdapter;
    ArrayList<Mahasiswa> mahasiswaList = new ArrayList<>();
    public static String TAG = "RV1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        rv1.setHasFixedSize(true);

        et_nama = findViewById(R.id.et_nama);
        et_nama2 = findViewById(R.id.et_nama2);
        btn_simpan = findViewById(R.id.btn_simpannn);

        btn_simpan.setOnClickListener(v ->{
            mahasiswaList.add(new Mahasiswa(et_nama.getText().toString(), et_nama2.getText().toString()));
            mahasiswaAdapter = new MahasiswaAdapter(this,mahasiswaList);
            rv1.setAdapter(mahasiswaAdapter);
        });
        mahasiswaList.add(new Mahasiswa("101010", "Viaa"));
        mahasiswaList.add(new Mahasiswa("101010", "Bima"));
        mahasiswaAdapter = new MahasiswaAdapter(this, mahasiswaList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        rv1.setLayoutManager(layoutManager);

        mahasiswaAdapter.setOnItemClickistener((position, v) -> {
            mahasiswaAdapter = new MahasiswaAdapter(this,mahasiswaList);
            rv1.setAdapter(mahasiswaAdapter);
        });
        rv1.setAdapter(mahasiswaAdapter);

//        ArrayList<Mahasiswa> data = getData();
//        MahasiswaAdapter adapter = new MahasiswaAdapter(this, data);
//        rv1.setAdapter(adapter);
//        rv1.setLayoutManager(new LinearLayoutManager(this));
    }
//
//    public ArrayList getData() {
//        ArrayList<Mahasiswa> data = new ArrayList<>();
//        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
//        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
//        for (int i = 0; i < nim.size(); i++) {
//            Mahasiswa mhs = new Mahasiswa();
//            mhs.nim = nim.get(i);
//            mhs.nama = nama.get(i);
//            Log.d(TAG,"getData "+mhs.nim);
//            data.add(mhs);
//        }
//        return data;
//    }

}